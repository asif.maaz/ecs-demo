variable "aws_region" {
  default = "ap-south-1"
}

variable "bucket_name" {
  default = "mytestbucket-90867"
}

variable "tf_state" {
  default = "tfstate-dev9091"
}
